{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Control.Monad.Trans.Bayes where
	
import Control.Monad.Trans.Class
import Control.Monad.Trans.Dist
import Control.Monad.Trans.Maybe

type FDist' = MaybeT FDist

instance Dist FDist' where
  weighted xws = lift (weighted xws)

bayes :: FDist' a -> [Perhaps a]
bayes = exact . onlyJust . runMaybeT

condition :: Bool -> FDist' ()
condition = MaybeT . return . toMaybe
  where toMaybe True  = Just ()
        toMaybe False = Nothing
