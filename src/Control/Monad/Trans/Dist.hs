{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
module Control.Monad.Trans.Dist where

import Data.List (foldl')
import Control.Monad
import Control.Monad.Trans.Class

type Weight = Float

class (Functor d, Monad d) => Dist d where
    weighted :: [(a, Weight)] -> d a

newtype Prob = P Float
  deriving (Eq, Ord, Num, Fractional)

instance Show Prob where
  show (P p) = show intPart ++ "." ++ show fracPart ++ "%"
    where digits = round (1000 * p)
          intPart = digits `div` 10
          fracPart = digits `mod` 10

data Perhaps a = Perhaps a Prob
  deriving (Eq, Ord, Show)

neverHappens :: Perhaps t -> Bool
neverHappens (Perhaps _ 0) = True
neverHappens _             = False

instance Functor Perhaps where
  fmap f (Perhaps x p) = Perhaps (f x) p

instance Monad Perhaps where
  return x = Perhaps x 1
  ph >>= f  | neverHappens ph  = never
            | otherwise        = Perhaps x (p1 * p2)
    where (Perhaps (Perhaps x p1) p2) = fmap f ph

class Monad m => MonadPerhaps m where
  perhaps :: a -> Prob -> m a
  never :: m a

instance MonadPerhaps Perhaps where
  never = Perhaps undefined 0
  perhaps = Perhaps

newtype PerhapsT m a = PerhapsT { runPerhapsT :: m (Perhaps a) }

instance MonadTrans PerhapsT where
  lift x = PerhapsT (liftM return x)

instance Monad m => Functor (PerhapsT m) where
  fmap = liftM

instance Monad m => Monad (PerhapsT m) where
  return = lift . return
  m >>= f = PerhapsT bound
    where bound = do
            ph <- runPerhapsT m
            case ph of
              (Perhaps x1 p1)  | p1 == 0    -> return never
                               | otherwise  -> do
                (Perhaps x2 p2) <- runPerhapsT (f x1)
                return (Perhaps x2 (p1 * p2))

type FDist = PerhapsT ([])

-- Create a uniform distribution for a number of elements
uniform :: Dist d => [a] -> d a
uniform = weighted . map (\x -> (x, 1))

exact :: FDist a -> [Perhaps a]
exact = runPerhapsT

instance Dist FDist where
  weighted [] = error "Empty distribution"
  weighted xws = PerhapsT (map weight xws)
    where weight (x,w) = Perhaps x (P (w / sum))
          sum = foldl' (+) 0 (map snd xws)


value :: Perhaps t -> t
value (Perhaps x _) = x

prob :: Perhaps t -> Prob
prob (Perhaps _ p) = p

catMaybes' :: [Perhaps (Maybe a)] -> [Perhaps a]
catMaybes' [] = []
catMaybes' (Perhaps Nothing _ : xs) =
  catMaybes' xs
catMaybes' (Perhaps (Just x) p : xs) =
  Perhaps x p : catMaybes' xs

onlyJust :: FDist (Maybe a) -> FDist a
onlyJust dist
    | total > 0 = PerhapsT (map adjust filtered)
    | otherwise = PerhapsT []
  where filtered = catMaybes' (runPerhapsT dist)
        total = sum (map prob filtered)
        adjust (Perhaps x p) =
          Perhaps x (p / total)
