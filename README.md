# README #

### What is this repository for? ###

* A probability distribution monad transformer, based on (http://www.randomhacks.net/2007/02/22/bayes-rule-and-drug-tests/)
* Version: 0.1

### How do I get set up? ###

* cabal configure
* cabal repl

### Who do I talk to? ###

* Owner: j.mueller.11@ucl.ac.uk